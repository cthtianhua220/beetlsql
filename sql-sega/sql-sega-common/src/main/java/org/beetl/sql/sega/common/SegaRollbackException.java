package org.beetl.sql.sega.common;

public class SegaRollbackException extends RuntimeException {
	public SegaRollbackException(String message ){
		super(message);
	}

}
