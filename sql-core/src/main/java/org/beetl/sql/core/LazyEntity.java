package org.beetl.sql.core;

public interface LazyEntity extends java.io.Serializable {
  Object get();

}
