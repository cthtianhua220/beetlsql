/*
 *  Copyright © 2020 - 2020 黄川 Rights Reserved.
 *  版权声明：黄川保留所有权利。
 *  免责声明：本规范是初步的，随时可能更改，恕不另行通知。黄川对此处包含的任何错误不承担任何责任。
 *  最后修改时间：2020/9/20 下午5:03
 */

package org.beetl.sql.id;

import lombok.Data;
import org.beetl.sql.annotation.entity.UpdateIgnore;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 黄川 huchuc@vip.qq.com
 * @date: 2020/9/13
 */
@Data
public abstract class BaseEntity implements Serializable {

}
