package org.beetl.sql.ext.solon.test.masterslave;

import org.beetl.sql.ext.solon.test.UserInfo;
import org.beetl.sql.mapper.BaseMapper;

public interface MasterSlaveUserInfoMapper extends BaseMapper<UserInfo> {
}
